
## Load Data functions

get_data_3D <- function(input_path,header){
  
  file_paths<- list.files(path=input_path,pattern="*.csv",full.names=T)
  layer_data <- list()
  data <-list()
  # Read data from each file and store it in the list
  for (i in 1:length(file_paths)) {
    layer_data[[i]] <- t(read.csv(file_paths[i], header = FALSE)) # the matrix is transposed to have the statistics as columns and tree intervals as rows
    colnames(layer_data[[i]])<-header
    # creation of the data frames containing all positions instead of intervals
    df <- data.table(layer_data[[i]])
    df$Start <- df$Start+1
    df_filled <- setDT(df)[, .(Position = Start:End, Sackin=Sackin, B2=B2,Polytomies=Polytomies ), .(Index=1:nrow(df))]
    data[[i]] <-df_filled
  }
  # Convert the list into a 3-dimensional array, array[positions][statistics][file]
  data_3D <- array(
    data = unlist(data),        # Unlist the data to form a vector
    dim = c(nrow(data[[1]]), ncol(data[[1]]), length(data)),  # Dimensions based on the first layer's dimensions
    dimnames = list(rownames(data[[1]]),colnames(data[[1]]),paste0("File", 1:length(data))
    )
  )
  return(data_3D)
}

get_data_w_3D <- function(input_path,header){
  
  file_paths<- list.files(path=input_path,pattern="*.csv",full.names=T)
  layer_data <- list()
  data <-list()
  # Read data from each file and store it in the list
  for (i in 1:length(file_paths)) {
    layer_data[[i]] <- t(read.csv(file_paths[i], header = FALSE)) # the matrix is transposed to have the statistics as columns and tree intervals as rows
    colnames(layer_data[[i]])<-header
    # creation of the data frames containing all positions instead of intervals
    df <- data.table(layer_data[[i]])
    df$Start <- df$Windows-499
    df_filled <- setDT(df)[, .(Position = Start:Windows, PI=PI, TajD=TajD,FST=FST ), .(Index=1:nrow(df))]
    data[[i]] <-df_filled
  }
  # Convert the list into a 3-dimensional array, array[positions][statistics][file]
  data_3D <- array(
    data = unlist(data),        # Unlist the data to form a vector
    dim = c(nrow(data[[1]]), ncol(data[[1]]), length(data)),  # Dimensions based on the first layer's dimensions
    dimnames = list(rownames(data[[1]]),colnames(data[[1]]),paste0("File", 1:length(data))
    )
  )
  return(data_3D)
}


get_all_data <- function(directory_path,file_paths,header,header_w){
  
  data_path_tree = paste(directory_path,"/",file_paths,"/stats/mono_pop_stats/",sep="")
  data_t <- get_data_3D(data_path_tree,header)
  
  data_path_w = paste(directory_path,"/",file_paths,"/stats/windows_stats/",sep="")
  data_w <- get_data_w_3D(data_path_w,header_w)
  
  
  # Remove Position and Index rows in the windowed data to avoid duplicates
  c_rm <-c("Position","Index")
  data_w <- data_w[,!(colnames(data_w) %in% c_rm),]
  
  # Bind the the 2 arrays in on single array
  array_all_data <- abind(data_t,data_w,along=2)
  
  return(array_all_data)
}


get_data_sites <- function(input_path,header){
  file_paths <-c()
  for (i in 1:length(input_path)) {
    file_paths = c(file_paths,paste(directory_path,"/",input_path[i],"/sites_selection.csv",sep=""))
  }
  print(file_paths)
  
  layer_data <- list()
  data <-list()
  # Read data from each file and store it in the list
  for (i in 1:length(file_paths)) {
    layer_data[[i]] <- read.csv(file_paths[i], header = TRUE,row.names = "X")
  }
  #print(layer_data)
  # Convert the list into a 3-dimensional array, array[positions][statistics][file]
  data_3D <- array(
    data = unlist(layer_data),        # Unlist the data to form a vector
    dim = c(nrow(layer_data[[1]]), ncol(layer_data[[1]]), length(layer_data)),  # Dimensions based on the first layer's dimensions
  dimnames = list(rownames(layer_data[[1]]),colnames(layer_data[[1]]),paste0(input_path))
  )
  return(data_3D)
}

