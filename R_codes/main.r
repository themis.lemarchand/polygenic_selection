
args <- commandArgs(trailingOnly = TRUE)
print(args)
print(class(args))

directory_path<-args

# data_path <- paste(directory_path,"/data_sim.RData",sep = "")
# 
# load(data_path)

library("abind")
library("dplyr")
library("ggplot2")
source("R_codes/fct_stats.r")
source("R_codes/fct_graphs.r")
source("R_codes/fct_load_data.r")
library("tidyr")
library("RColorBrewer")


### Load the data ###


file_paths<- list.files(path=directory_path,pattern="cp|selection*",full.names=F)

header <- read.csv(paste(directory_path,"/cp/stats/header/1.csv",sep = ""),header=FALSE)
header_w <- read.csv(paste(directory_path,"/cp/stats/header/1_w.csv",sep = ""),header=FALSE)

# Get the data for each simulation in a 3D array
for (i in 1:length(file_paths)) {
  name <- paste(file_paths[i],"_","data3D",sep="")
  assign(name,get_all_data(directory_path,file_paths[i],header,header_w))
}

# Get the mean for each statistics at each position across the replicates in a data frame
for (i in 1:length(file_paths)) {
  name_input <- paste(file_paths[i],"_data3D",sep="")
  name_output <- paste(file_paths[i],"_means",sep="")

  assign(name_output,apply(get(name_input), MARGIN = c(1,2), FUN = function(x) mean(x, na.rm = TRUE)))
  assign(name_output,data.frame(get(name_output)))
}

print("Missing Values in cp means + data3D")

 missing_values <-apply(cp_means,2,function(x) which(is.nan(x)))
 print(missing_values)

 missing_values <-apply(cp_data3D,2,function(x) which(is.nan(x)))
 print(missing_values)


### Statistics ###

# Indicate the wanted threshold for each statistics
dict_threshold <- c(Sackin=0.95,B2=0.05,TajD=0.05,Polytomies=0.95,FST=0.95,PI=0.05)

# Initiate the positions of the sites of interest
sitepos = c(seq(10000,190000,20000))

# # Get the threshold value for every statistics based on the positive control and save it into a csv file
output_path=paste(directory_path,"/plots/",sep="")
df<-data.frame()
for (i in 3:length(colnames(cp_means))) {
  threshold <- get_threshold_stat(sitepos,colnames(cp_means)[i],dict_threshold[colnames(cp_means)[i]],output_path)
  df[1,colnames(cp_means)[i]]<- unname(threshold)
}
threshold_stats <-df[,!names(df)=="X"]

write.csv(threshold_stats,file=paste(directory_path,"/threshold_stats.csv",sep=""))



# Get the number of sites under selection for each statistics of each replicates of each simulation and store them into a csv file per simulation
## Loop on every simulations
for (i in 1:length(file_paths) ){
  name <- paste(file_paths[i],"_data3D",sep="")
  sim <- get(name)
  output_path = paste(directory_path,"/",file_paths[i],sep="")
  save_sites_selection(sim,output_path,threshold_stats,dict_threshold)
}

sites_cp <-read.csv(file=paste(directory_path,"/cp/sites_selection.csv",sep=""),header=TRUE,row.names = "X")

selec_paths <- file_paths[-1]

sites_s <- get_data_sites(selec_paths,header)


threshold_nb_sites = 1

contingency_table <- data.frame()

for (k in 1: length(colnames(sites_cp))){
  stat=colnames(sites_cp[k])
  nb_sel_rep = sum(sites_cp[,k]>=threshold_nb_sites,na.rm=TRUE)
  print(nb_sel_rep)
  # Total number of replicates within a simulation
  nb_replicates = length(sites_cp[,k])
  print(nb_replicates)
  param <- gsub("output/","",directory_path)
  contingency_table <- rbind(contingency_table,c("control",param,stat,dict_threshold[stat],threshold_nb_sites,nb_sel_rep,nb_replicates))
}
colnames(contingency_table)<- c("Simulation","Parameters","Stat","Threshold_stat","Threshold_nb_sites","Nb_rep_selection","Nb_replicates")

# Loop on every simulation
for (i in 1:length(selec_paths) ){
  # Loop on every stat
  for (j in 1: length(colnames(sites_s[,,i]))){
    stat=colnames(sites_s[,,i])[j]
    print(stat)
    #number of replicates considered under selection
    nb_sel_rep = sum(sites_s[,j,i]>=threshold_nb_sites,na.rm=TRUE)
    print(nb_sel_rep)
    # Total number of replicates within a simulation
    nb_replicates = length(sites_s[,j,i])
    print(nb_replicates)
    param <- gsub("output/","",directory_path)
    contingency_table <- rbind(contingency_table,c(selec_paths[i],param,stat,dict_threshold[stat],threshold_nb_sites,nb_sel_rep,nb_replicates))
  }
}


write.csv(contingency_table,paste(directory_path,"/contingency_table",sep=""))

### Plot the data ###

dir.create(paste(directory_path,"/plots",sep=""))

# Plot all statistics
for (i in 3:length(colnames(cp_means))) {
  print(colnames(cp_means)[i])
  output_path=paste(directory_path,"/plots/",colnames(cp_means)[i],sep="")
  dir.create(output_path,recursive=TRUE)

  plot_boxplot(output_path,colnames(cp_means)[i],selection_OPT10_sigma7_means)
  plot_distribution_site(output_path,colnames(cp_means)[i],sitepos,selection_OPT10_sigma7_means)
  plot_distribution_genome(output_path,colnames(cp_means)[i],sitepos,selection_OPT10_sigma7_means)
  plot_distribution(output_path,colnames(cp_means)[i],selection_OPT10_sigma7_means)
}

## Plot all simulations at once
output_path=paste(directory_path,"/plots/",sep="")

opt10s7 = paste("OPT=10 ","sigma=7",sep="")
opt5s7 = paste("OPT=5 ","sigma=7",sep="")
opt5s5 = paste("OPT=5 ","sigma=5",sep="")
opt5s10 = paste("OPT=5 ","sigma=10",sep="")
opt2s7 = paste("OPT=2 ","sigma=7",sep="")

custom_colors <- c( "#ff7f0e", "#2ca02c","red","blue","purple","darkblue")

all_objects<-ls()
df_temp <- all_objects[sapply(all_objects,function(x) is.data.frame(get(x)))]
df_to_plot <- df_temp[grep("*means",df_temp)]

df_list <- list()

for (df_name in df_to_plot){
  df<-get(df_name)
  df$source<-df_name
  df_list[[df_name]]<-df
}
combined_df <- bind_rows(df_list)

names<-colnames(combined_df)[4:length(colnames(combined_df))-1]

combined_df$source <- factor(combined_df$source, levels = c("cp_means", "selection_OPT10_sigma7_means", "selection_OPT5_sigma10_means","selection_OPT5_sigma7_means","selection_OPT5_sigma5_means","selection_OPT2_sigma7_means"),
                         labels = c("Control" ,opt10s7, opt5s10,opt5s7,opt5s5,opt2s7))

for(stat in names){
  plot_along_genome(combined_df,"_means_",output_path,custom_colors,stat)
  plot_boxplot2(combined_df,"_means_",output_path,custom_colors,stat)
}

# Plotting raw data

all_objects<-ls()
df_temp <- all_objects[sapply(all_objects,function(x) is.array(get(x)))]
df_to_plot <- df_temp[grep("*3D",df_temp)]

for (stat in names){
  df_list <- list()
  for (df_name in df_to_plot){
    df<-get(df_name)
    df_st<-data.frame(df[sitepos,stat,])
    df_st$source<-df_name
    df_list[[df_name]]<-df_st
  }
  combined_df_2 <- bind_rows(df_list)
  df_test <-combined_df_2 %>%  pivot_longer(cols = -source, names_to = "Variable", values_to = stat)
  df_test<-data.frame(df_test)
  df_test$source <- factor(df_test$source, levels = c("cp_data3D", "selection_OPT10_sigma7_data3D", "selection_OPT5_sigma10_data3D","selection_OPT5_sigma7_data3D","selection_OPT5_sigma5_data3D","selection_OPT2_sigma7_data3D"),
                           labels = c("Control" ,opt10s7, opt5s10,opt5s7,opt5s5,opt2s7))
  plot_boxplot2(df_test,"_raw_sites",output_path,custom_colors,stat)
}


save.image(paste(directory_path,"/data_sim.RData",sep = ""))


print("DONE")
