library(data.table)


temp = list.files(path="./output_stats/fst/",pattern="*.csv",full.names=T)
myfiles = lapply(temp,fread)
df = do.call(cbind,myfiles)
tab=rowMeans(df,na.rm=TRUE)
pdf("fst.pdf")
#plot(tab,type="l",col="blue")
temp1 = list.files(path="./output_stats/fst_ctrl/",pattern="*.csv",full.names=T)
myfiles1 = lapply(temp1,fread)
df1 = do.call(cbind,myfiles1)
print(df1)
tab1 = rowMeans(df1,na.rm=TRUE)
print(tab1)
print(tab)
plot(tab1,type="l",col="red")
lines(tab,col="blue")
dev.off()





