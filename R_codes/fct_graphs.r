
library(data.table)
library(tidyr)
library(ggplot2)

# Define plots

plot_distribution <- function(output_path,stat,ref_selection){
  pdf(paste(output_path,"/",stat,"_distribution.pdf",sep=""))
  
  plot(density(ref_selection[[stat]]),col="blue",ylab="Density",xlab=stat,main=paste(stat,"distribution"),
       ylim=c(min(density(ref_selection[[stat]])$y,density(cp_means[[stat]])$y),max(density(ref_selection[[stat]])$y,density(cp_means[[stat]])$y)),
       xlim=c(min(density(ref_selection[[stat]])$x,density(cp_means[[stat]])$x),max(density(ref_selection[[stat]])$x,density(cp_means[[stat]])$x)))
  lines(density(cp_means[[stat]]),col="red")
  
  par(mar = c(2,3,4,4),
      xpd = TRUE)
  legend(x = "bottomleft",
         inset = c(-0.15, -0.2),
         legend = c("Selection","Control"),
         col = c("blue","red"),
         xpd = NA,
         lwd = 2,
         box.col = "white")
  dev.off()
}


plot_distribution_genome <- function(output_path,stat,sitepos,ref_selection){
  pdf(paste(output_path,"/",stat,"_along_genome.pdf",sep=""))
  plot(ref_selection$Position,ref_selection[[stat]],col="blue",ylab=stat,xlab="Position",main=paste(stat,"distribution troughout the genome"),
       type="l",ylim=c(min(cp_means[[stat]],ref_selection[[stat]]),max(cp_means[[stat]],ref_selection[[stat]])))
  
  lines(cp_means[[stat]],col="red")
  abline(v=sitepos,lty=2)
  
  par(mar = c(2,3,4,4),
      xpd = TRUE)
  legend(x = "bottomleft",
         inset = c(-0.15, -0.2),
         legend = c("Selection","Control"),
         col = c("blue","red"),
         xpd = NA,
         lwd = 2,
         box.col = "white")
  dev.off()
}

plot_distribution_site <- function(output_path,stat,sitepos,ref_selection){
  
  pdf(paste(output_path,"/",stat,"_distribution_site.pdf",sep=""))
  
  plot(density(ref_selection[[stat]][sitepos]),col="blue",
       xlim=c(min(c(density(ref_selection[[stat]][sitepos])$x,density(cp_means[[stat]][sitepos])$x)),max(c(density(ref_selection[[stat]][sitepos])$x,density(cp_means[[stat]][sitepos])$x))),
       ylim=c(min(c(density(ref_selection[[stat]][sitepos])$y,density(cp_means[[stat]][sitepos])$y)),max(c(density(ref_selection[[stat]][sitepos])$y,density(cp_means[[stat]][sitepos])$y))),
                  ylab="Density",xlab=stat,main=paste(stat,"distribution at inserted site"))
  lines(density(cp_means[[stat]][sitepos]),col="red")
  par(mar = c(2,3,4,4),
      xpd = TRUE)
  legend(x = "bottomleft",
         inset = c(-0.15, -0.2),
         legend = c("Selection","Control"),
         col = c("blue","red"),
         xpd = NA,
         lwd = 2,
         box.col = "white")
  
  dev.off()
}

plot_boxplot <- function(output_path,stat,ref_selection){
  pdf(paste(output_path,"/",stat,"_box_plot.pdf",sep=""))
  
  boxplot(ref_selection[[stat]],cp_means[[stat]],xlab="Population",ylab=stat,main=paste(stat,"troughout the genome"),col=c("blue","red"))
  
  par(mar = c(2,3,4,4),
      xpd = TRUE)
  legend(x = "bottomleft",
         inset = c(-0.15, -0.2),
         legend = c("Selection","Control"),
         col = c("blue","red"),
         xpd = NA,
         lwd = 2,
         box.col = "white")
  dev.off()
}



plot_along_genome<-function(df,input_type,output_path,custom_colors,stat){
  
  file_path <- file.path(output_path, paste0(stat,input_type, "distribution.pdf"))
  pdf(file = file_path,width=10,height = 6)
  
  p <- ggplot(df, aes(x = Position, y = get(stat), color = source)) +
    geom_line() +
    geom_vline(xintercept = sitepos, linetype = "dashed", color = "black") +  # Add vertical lines
    theme(  
      panel.grid.major = element_blank(), # Remove major grid lines
      panel.grid.minor = element_blank() , # Remove minor grid lines
      panel.background = element_rect(fill = "white"),  # Set background color to white
      plot.background = element_rect(fill = "white") ,   # Set plot background color to white
      plot.margin=unit(c(0.5,0,0.5,0.5),"cm"),
      plot.title=element_text(hjust = 0.5),
      legend.box.margin=margin(0,0,0,0)
    ) +
    scale_color_manual(values = custom_colors)+
    labs(
      title = paste(stat, "along the genome"),  # More descriptive title
      x = "Position",                   # Label for the x-axis
      y = stat,                            # Label for the y-axis
      color = "Simulation"                  # Legend title for the color aesthetic
    ) 
  
  print(p)
  dev.off()
}

plot_boxplot2<-function(df,input_type,output_path,custom_colors,stat){
  file_path <- file.path(output_path, paste0(stat,input_type,"boxplot.pdf"))
  pdf(file = file_path,width=10,height = 6)
  
  p <- ggplot(df, aes(x = source, y = get(stat), color = source)) +
    geom_boxplot()+
    theme_minimal() +
    scale_color_manual(values = custom_colors)+
    labs(
      title = paste(stat, " boxplot"),  # More descriptive title
      x = "Simulation",                   # Label for the x-axis
      y = stat,                            # Label for the y-axis
      color = "Simulation"                  # Legend title for the color aesthetic
    ) 
  
  print(p)
  dev.off()
}