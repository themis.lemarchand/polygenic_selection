
# Get the threshold value of a statistic based on the wanted associated p.value threshold (0.05,0.01 etc)
get_threshold_stat <- function(sitepos,stat,threshold_p,output_path){ 
  data <- cp_data3D[sitepos,stat,]
  threshold <- quantile(data,probs=threshold_p,na.rm=TRUE)
  
  pdf(paste(output_path,stat,"_hist.pdf",sep=""))
  print(paste(output_path,stat,"_hist.pdf",sep=""))
  hist(data,breaks=100,main=stat)
  #print(data)
  abline(v=threshold,lty=2)
  dev.off()
  
  return(threshold)
}

# Count the number of sites under selection considering a specific threshold
site_selection <- function(data_input,stat,sites,threshold_stat,dict_threshold,i){ 
  data = data_input[sitepos,stat,i]

  if (dict_threshold  > 0.9 ){
    processed_data <- data >= threshold_stat[1,1]
  } else {
    processed_data <- data <= threshold_stat[1,1]
  }
  count_stat <- sum(processed_data)
  
  return(count_stat)
}

# Get the number of sites under selection for each statistics and each replicates and save them into a csv file
save_sites_selection<- function(sim,output_path,threshold_stats,dict_threshold){
  df <- data.frame()
  # Loop on every stat
  for(j in 3:length(colnames(sim))){
    stat <- colnames(sim)[j]
    # Loop on every file (replicate)
    for (k in 1:dim(sim)[3]) {
      #print(k)
      #print(stat)
      df[k,stat] <- site_selection(sim,stat,sitepos,threshold_stats[stat],unname(dict_threshold[stat]),k)
    }
  }
  print(df)
  write.csv(df,file=paste(output_path,"/sites_selection.csv",sep=""))
}
