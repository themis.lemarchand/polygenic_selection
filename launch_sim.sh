#!/bin/bash


##partition type
#SBATCH --partition=fast

## Nombre de Noeuds
#SBATCH --nodes=1

## Nombre de processeur par noeud
#SBATCH --ntasks-per-node=1

## Nom du job
#SBATCH --job-name=EssaiSLIM

## Nom des fichiers de sorties standard et erreur
#SBATCH --output=/shared/projects/polygenicsln/tlemarchand/current_pipeline/logs/%x.%j.out
#SBATCH --error=/shared/projects/polygenicsln/tlemarchand/current_pipeline/logs/%x.%j.err

## Quantité de RAM par noeud
#SBATCH --mem-per-cpu=4GB

## Temps limite pour lancer le job
#SBATCH --time=00-04:00:00 # days-hh:mm:ss

## Projet par default
#SBATCH --account=polygenicsln

#SBATCH --array=1-300

#find output/* -type f -delete

module load slim/4.0.1
module load python/3.9

slim -d REPID=$SLURM_ARRAY_TASK_ID slim_simu/selection.slim
slim -d REPID=$SLURM_ARRAY_TASK_ID -d OPT=10 slim_simu/selection.slim 
slim -d REPID=$SLURM_ARRAY_TASK_ID -d OPT=2 slim_simu/selection.slim 
slim -d REPID=$SLURM_ARRAY_TASK_ID -d sigma=5 slim_simu/selection.slim
slim -d REPID=$SLURM_ARRAY_TASK_ID -d sigma=10 slim_simu/selection.slim 


#slim -d REPID=$SLURM_ARRAY_TASK_ID slim_simu/controle.slim





