# polygenic_selection
This is a pipeline for the simulation and detection of polygenic selection edited by Thémis Lemarchand.
All scripts has been run on the IFB-core Cluster of the French Institute of Bioinformatics using SLURM 22.05.9.
Therefore we launch the different steps of the pipeline using the scripts : launch_*.sh. These script will load the necessary modules, execute the scripts and manage the output.
One module cannot be loaded and must be installed in the current directory with : pip install pyslim==1.0.4

- launch_sim.sh : launch the simulations with SLiM
- launch_rec.sh : launch the recapitaion and overlay processes using python
- launch_stats.sh : launch the calculations of the different statistics on the recapitated trees in python.
- launch_r.sh : launch the calculation of the contingency table and plot the statistical data.

To run a script use the sbatch command, ex: sbatch launch_sim.sh

- "/logs" : contains the outputs and errors
- "/output" : contains the results of the simulations, statistics and plots
- "/slim_simu" : contains the SLiM scripts for the simulations control and under selection
- "/python_codes": contains the scripts for the recapitaion and the calculation of the statistics
- "/R_codes" : contains the R codes to compute the plots and contingency table

