#!/bin/bash


##partition type
#SBATCH --partition=fast

## Nombre de Noeuds
#SBATCH --nodes=1

## Nombre de processeur par noeud
#SBATCH --ntasks-per-node=1

## Nom du job
#SBATCH --job-name=EssaiSLIM

## Nom des fichiers de sorties standard et erreur
#SBATCH --output=/shared/projects/polygenicsln/tlemarchand/current_pipeline/logs/%x.%j.out
#SBATCH --error=/shared/projects/polygenicsln/tlemarchand/current_pipeline/logs/%x.%j.err

## Quantité de RAM par noeud
#SBATCH --mem-per-cpu=140GB

## Temps limite pour lancer le job
#SBATCH --time=00-07:00:00 # days-hh:mm:ss

## Projet par default
#SBATCH --account=polygenicsln

module load r

for dir in output/* ; do
	echo "File: $dir "
	Rscript R_codes/main.R $dir
	done
