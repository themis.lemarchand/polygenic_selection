#!/bin/bash

##partition type
#SBATCH --partition=fast

## Nombre de Noeuds
#SBATCH --nodes=1

## Nombre de processeur par noeud
#SBATCH --ntasks-per-node=1

## Nom du job
#SBATCH --job-name=EssaiSLIM

## Nom des fichiers de sorties standard et erreur
#SBATCH --output=/shared/projects/polygenicsln/tlemarchand/current_pipeline/logs/%x.%j.out
#SBATCH --error=/shared/projects/polygenicsln/tlemarchand/current_pipeline/logs/%x.%j.err

## Quantité de RAM par noeud
#SBATCH --mem-per-cpu=4GB

## Temps limite pour lancer le job
#SBATCH --time=00-04:00:00 # days-hh:mm:ss

## Projet par default
#SBATCH --account=polygenicsln

#SBATCH --array=1-300

module load python/3.9

module load tskit/0.5.3
module load msprime/1.2.0


sim_type="selec"

for dir in output/* ; do
	echo "Dir : $dir "
	for dir2 in $dir/*; do
		echo " sub dir : $dir2"
		name=$(basename "$dir2")
		echo "dir name: $name"
		if [[ "$name" == *"$sim_type"* ]]; then
			echo "$sim_type ok "
			python python_codes/recapitation.py "$SLURM_ARRAY_TASK_ID" "$dir2" "$dir2/recap_trees/"
		fi
		done
	
	done
