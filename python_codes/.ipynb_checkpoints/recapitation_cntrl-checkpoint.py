
#!/usr/bin/python
 
import numpy as np
import sys

import tskit
import msprime
import pyslim


#Charging the treeSequence file(ts)

tree_name_cntrl = str("output/trees/replica_controle_N"+sys.argv[1]+".trees")




ts_cntrl = tskit.load(tree_name_cntrl)


#Extract the ends and rates to set the recombination and mutation map
positions=[]
rates=[]
mutpos=[]
mutrate=[]

with open('output/ends_c.txt', 'r') as file:
    for line in file: 
        components = line.split()
    for cmp in components:
        positions.append(float(cmp))
with open('output/rate_c.txt', 'r') as file1:
    for line in file1:
        components1 = line.split()
    for cmp1 in components1:
        rates.append(float(cmp1))

components = ""
components1 = ""

with open('output/mut_ends_c.txt', 'r') as file:
    for line in file:
        components = line.split()
    for cmp2 in components:
        mutpos.append(float(cmp2))
        
with open('output/mut_rate_c.txt', 'r') as file1:
    for line in file1:
        components1 = line.split()
    for cmp1 in components1:
        mutrate.append(float(cmp1))


##A modifier
#while len(mutrate)<len(components)-2:
#    mutrate.append(1e-7)
#    mutrate.append(0.0)

#while len(mutrate)<len(components):
#    mutrate.append(1e-7)

mutrate.insert(0,1e-7)
mutrate= mutrate[0:len(mutrate)-1]

# step 1
positions.insert(0, 0)
# step 2
positions[-1] += 1
assert positions[-1] == ts_cntrl.sequence_length

mutpos.insert(0,0)
mutpos[-1] +=1
assert mutpos[-1] ==ts_cntrl.sequence_length


#Setting the RecombinationMap
recomb_map = msprime.RateMap(position=positions, rate=rates)
mutation_map = msprime.RateMap(position=mutpos, rate=mutrate)

print(recomb_map)
print(mutation_map)
#Recapitation

rts_cntrl = pyslim.recapitate(ts_cntrl,ancestral_Ne=1000,recombination_rate = recomb_map,random_seed=1)

#Choosing individuals that are still alive
rng = np.random.default_rng()
#Warning this gives IDS not individual IDS !

alive_cntrl = pyslim.individuals_alive_at(rts_cntrl,0)

keep_cntrl = alive_cntrl
#keep_cntrl = rng.choice(alive_inds,100,replace=False)


keep_cntrl1 = rng.choice(rts_cntrl.samples(population=1),1000,replace=False)
keep_cntrl2 = rng.choice(rts_cntrl.samples(population=2),1000,replace=False)

keep_nodes_cntrl=[]
for i in keep_cntrl:
    keep_nodes_cntrl.extend(rts_cntrl.individual(i).nodes)
recap_sampled_cntrl = rts_cntrl.simplify(keep_nodes_cntrl,keep_input_roots=True)

popu1_cntrl = rts_cntrl.simplify(keep_cntrl1, keep_input_roots=True)
popu2_cntrl = rts_cntrl.simplify(keep_cntrl2, keep_input_roots=True)

print(mutation_map)

#Adding neutral mutations(with mutation rate mu)

next_id = pyslim.next_slim_mutation_id(recap_sampled_cntrl)
ts_cntrl = msprime.sim_mutations(
           recap_sampled_cntrl,
           rate=mutation_map,
           model=msprime.SLiMMutationModel(type=0, next_id=next_id),
           keep=True,
)



next_id = pyslim.next_slim_mutation_id(popu1_cntrl)
popu1_cntrl = msprime.sim_mutations(
           popu1_cntrl,
           rate=mutation_map,
           model=msprime.SLiMMutationModel(type=0, next_id=next_id),
           keep=True,
)

next_id = pyslim.next_slim_mutation_id(popu2_cntrl)
popu2_cntrl = msprime.sim_mutations(
           popu2_cntrl,
           rate=mutation_map,
           model=msprime.SLiMMutationModel(type=0, next_id=next_id),
           keep=True,
)


ts_cntrl.dump("output/recap_trees/full_recap_cntrl"+sys.argv[1]+".trees")
popu1_cntrl.dump("output/recap_trees/popu1_cntrl"+sys.argv[1]+".trees")
popu2_cntrl.dump("output/recap_trees/popu2_cntrl"+sys.argv[1]+".trees")