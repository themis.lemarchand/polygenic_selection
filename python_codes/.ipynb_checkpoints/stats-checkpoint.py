import numpy as np

import tskit
import msprime
import pyslim
import sys
from numpy import savetxt

#ts1 = tskit.load("output/recap_trees/popu1_"+sys.argv[1]+"recap.trees")
#ts2 = tskit.load("output/recap_trees/popu2_"+sys.argv[1]+"recap.trees")
ts = tskit.load("output/recap_trees/full_recap_"+sys.argv[1]+".trees")
ts_cntrl = tskit.load("output/recap_trees/full_recap_cntrl"+sys.argv[1]+".trees")
#windows = np.linspace(0, ts.sequence_length, num=80)
#windows_cntrl = np.linspace(0, ts_cntrl.sequence_length,num=80)

windows = np.arange(0,40500,500)
#windows = np.append(windows,)

#AFS
afs1 = ts.allele_frequency_spectrum(sample_sets=[ts.samples(population=0)],span_normalise=True)[1:(len(ts.samples(population=0))-1)]
afs2 = ts.allele_frequency_spectrum(sample_sets=[ts.samples(population=1)],span_normalise=True)[1:(len(ts.samples(population=1))-1)]

a = 0
for i in range(1,len(afs1)):
    a = a + afs1[i]
afs1 = afs1/(1/a)



#FST
fst1 = ts.Fst(sample_sets=[ts.samples(population=0),ts.samples(population=1)],mode="site",windows=windows)
fst2 = ts_cntrl.Fst(sample_sets=[ts_cntrl.samples(population=0),ts_cntrl.samples(population=1)],mode="site",windows=windows)
#PI
pi1 = ts.diversity(ts.samples(population=0),mode="site",windows=windows,span_normalise=True)
pi2 = ts.diversity(ts.samples(population=1),mode="site",windows=windows,span_normalise=True)

#Tajimas_D
taj_d1 = ts.Tajimas_D(ts.samples(population=0),windows=windows)
taj_d2 = ts.Tajimas_D(ts.samples(population=1),windows=windows)
#taj_d1 = ts.Tajimas_D(windows=windows)
#taj_d2 = ts2.Tajimas_D(windows=windows)
print(taj_d1)
print(taj_d2)
savetxt("R_codes/output_stats/popu1_tajd/"+sys.argv[1]+".csv",taj_d1,delimiter=' ')
savetxt("R_codes/output_stats/popu1_afs/"+sys.argv[1]+".csv",afs1,delimiter=' ')
savetxt("R_codes/output_stats/popu1_pi/"+sys.argv[1]+".csv",pi1,delimiter=' ')
savetxt("R_codes/output_stats/popu2_tajd/"+sys.argv[1]+".csv",taj_d2,delimiter=' ')
savetxt("R_codes/output_stats/popu2_afs/"+sys.argv[1]+".csv",afs2,delimiter=' ')
savetxt("R_codes/output_stats/popu2_pi/"+sys.argv[1]+".csv",pi2,delimiter=' ')
savetxt("R_codes/output_stats/fst/"+sys.argv[1]+".csv",fst1,delimiter=' ')
savetxt("R_codes/output_stats/fst_ctrl/"+sys.argv[1]+".csv",fst2,delimiter=' ')


