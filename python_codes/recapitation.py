#!/usr/bin/python
 
import numpy as np
import sys
import tskit
import msprime
import pyslim
import json
import os
import warnings

warnings.simplefilter('ignore', msprime.TimeUnitsMismatchWarning)


# Load the simulation parameters to get the number of individuals
with open(sys.argv[2]+"/parameters/parameters.txt","r") as file:
    for line in file :
        param = json.loads(line)

ind = param["Ind"][0]

# Load the treeSequence file, sys.argv[1] = replicate ID, sys.argv[2] = input folder, sys.argv[3] = output folder
tree_name = str(sys.argv[2]+"/trees/replica_N"+sys.argv[1]+".trees")
ts = tskit.load(tree_name) 

# Extract the ends and rates to set the recombination and mutation map
positions=[]
rates=[]
mutpos=[]
mutrate=[]

with open(sys.argv[2]+'/parameters/ends.txt', 'r') as file:
    for line in file: 
        components = line.split()
        #print("cmp : ",components)
    for cmp in components:
        positions.append(float(cmp))
        
with open(sys.argv[2]+'/parameters/rate.txt', 'r') as file1:
    for line in file1:
        components1 = line.split()
        #print("cmp1 : ",components1)
    for cmp1 in components1:
        rates.append(float(cmp1))
        
components = ""
components1 = ""

with open(sys.argv[2]+'/parameters/mut_ends.txt', 'r') as file:
    for line in file:
        components = line.split()
    for cmp2 in components:
        mutpos.append(float(cmp2))
        
with open(sys.argv[2]+'/parameters/mut_rate.txt', 'r') as file1:
    for line in file1:
        components1 = line.split()
    for cmp1 in components1:
        mutrate.append(float(cmp1))


# Add one set for the 0 we are adding to determine the beginning of the genome
mutrate.insert(0,mutrate[1])
mutrate = mutrate[0:len(mutrate)-1]

# Add 0 to the indexes for the map
positions.insert(0,0)
positions[-1] += 1
assert positions[-1] == ts.sequence_length

# Same for the mutation indexes
mutpos.insert(0,0)
mutpos[-1]+=1
assert mutpos[-1] == ts.sequence_length

# Setting the RecombinationMaps
recomb_map = msprime.RateMap(position=positions, rate=rates)
mutation_map = msprime.RateMap(position=mutpos, rate=mutrate)

# Recapitation
rts = pyslim.recapitate(ts, ancestral_Ne = ind,recombination_rate=recomb_map,random_seed=1)

# Choosing individuals that are still alive
# Warning this gives IDS not individual IDS !
alive_pop1 = pyslim.individuals_alive_at(rts,0,population=1)
alive_pop2 = pyslim.individuals_alive_at(rts,0,population=2)

# Randomly sample individuals
rng = np.random.default_rng()
keep_indivs1 = rng.choice(alive_pop1,100,replace=False)
keep_indivs2 = rng.choice(alive_pop2,100,replace=False)

keep_indivs = np.concatenate((keep_indivs1,keep_indivs2))

# Simplify every indiv kept in whole population, loop needed to get the indiv ID instead of node ID
keep_nodes=[]
for i in keep_indivs:
	keep_nodes.extend(rts.individual(i).nodes)
recap_sampled_ts = rts.simplify(keep_nodes,keep_input_roots=True)

# Add neutral mutations(with mutation rate mu)
next_id = pyslim.next_slim_mutation_id(recap_sampled_ts)
mts = msprime.sim_mutations(
           recap_sampled_ts,
           rate=mutation_map,
           model=msprime.SLiMMutationModel(type=0, next_id=next_id),
           keep=True,
)

# Extract population 1 tree and population 2 tree from the whole tree
samples_p1 = mts.samples(population=0)
mts_p1 = mts.simplify(samples=samples_p1,keep_input_roots=True)

samples_p2 = mts.samples(population=1)
mts_p2 = mts.simplify(samples=samples_p2,keep_input_roots=True)

# Create the output directory 
os.makedirs(sys.argv[3],exist_ok=True)
    
# Dump the recapitation process
mts.dump(sys.argv[3]+"full_"+sys.argv[1]+"_recap.trees")
mts_p1.dump(sys.argv[3]+"popu1_"+sys.argv[1]+"_recap.trees")
mts_p2.dump(sys.argv[3]+"popu2_"+sys.argv[1]+"_recap.trees")
