
import pyslim
import tskit 
import sys
from numpy import savetxt
import numpy as np
import json
import csv
import stats_fct as st
import os

print(sys.argv)
print("sim type : ",sys.argv[3])
print(type(sys.argv[3]))

directories = os.listdir(sys.argv[2])

for dir in directories:
    print("path :", sys.argv[2]+"/"+dir+"/recap_trees/popu1_"+sys.argv[1]+"_recap.trees") 
    if sys.argv[3] in dir:
        print("pattern found :" ,sys.argv[3])
    
        # Load the trees
        tree = tskit.load(sys.argv[2]+"/"+dir+"/recap_trees/popu1_"+sys.argv[1]+"_recap.trees")
        full_tree = tskit.load(sys.argv[2]+"/"+dir+"/recap_trees/full_"+sys.argv[1]+"_recap.trees")
        
        # Get the position start/end of each tree for a treesequence with one population
        trees_stats = st.treesPositions(tree)
        #print(tree_stats)
        
        # Create a list containing the names of the statistics computed, it will store the order of the caluculated satistics to retreive them correctly in a dataframe. "Start" and "End" are calculated just before in st.treesPositions()
        header_tree = ["Start","End"]
        header_w = ["Windows"]
        
        # Load parameters 
        
        with open(sys.argv[2]+"/"+dir+"/parameters/parameters.txt","r") as file:
            for line in file :
                param = json.loads(line)
        seq_length = param["X"][0]*param["N"][0]

        if(len(sys.argv)>4):
            window_size = int(sys.argv[4])
        else :
            window_size = 500 #default value        
        print("window", window_size)

        #create windows to move accross genome
        windows = np.arange(0,seq_length+window_size,window_size)

        ## Polytomies
        header_tree.append("Polytomies")
        trees_stats.append(st.polytomies(tree))
        
        ## Sackin index
        header_tree.append("Sackin")
        trees_stats.append(st.sackin(tree,trees_stats[0]))
        
        ## B2	
        header_tree.append("B2")
        trees_stats.append(st.b2(tree,trees_stats[0]))
        
        w = windows.tolist()
        w=w[1:]
        windows_stats = [w]
        
        ##PI
        header_w.append("PI")
        windows_stats.append(tree.diversity(mode="site",windows=windows,span_normalise=True).tolist())
        
        ##Tajimas_D
        header_w.append("TajD")
        windows_stats.append(tree.Tajimas_D(windows=windows).tolist())
        
        #FST
        header_w.append("FST")
        windows_stats.append(full_tree.Fst(sample_sets=[full_tree.samples(population=0),full_tree.samples(population=1)],mode="site",windows=windows).tolist())
      
        # Create the output directory 
        os.makedirs(sys.argv[2]+"/"+dir+"/stats/",exist_ok=True)
        os.makedirs(sys.argv[2]+"/"+dir+"/stats/header/",exist_ok=True)
        os.makedirs(sys.argv[2]+"/"+dir+"/stats/mono_pop_stats/",exist_ok=True)
        os.makedirs(sys.argv[2]+"/"+dir+"/stats/windows_stats/",exist_ok=True)
        
        print(header_w)   
        #print(windows_stats)
        
        #print(len(windows_stats[0]),len(windows_stats[1]),len(windows_stats[2]),len(windows_stats[3]))
        
        ## Save the results in csv files    
        with open(sys.argv[2]+"/"+dir+"/stats/header/"+sys.argv[1]+".csv","w",newline="") as file:
            csv.writer(file).writerow(header_tree)
            
        with open(sys.argv[2]+"/"+dir+"/stats/header/"+sys.argv[1]+"_w.csv","w",newline="") as file:
            csv.writer(file).writerow(header_w)
        savetxt(sys.argv[2]+"/"+dir+"/stats/mono_pop_stats/"+sys.argv[1]+".csv",trees_stats,delimiter=',')

        savetxt(sys.argv[2]+"/"+dir+"/stats/windows_stats/"+sys.argv[1]+".csv",windows_stats,delimiter=',')

print("DONE")

