
import pyslim
import tskit
import sys
from numpy import savetxt
import numpy as np


# Function getting the positions start/end of each tree for a treesequence
def treesPositions(treesequence):
    tree_index =[]
    tree_start =[]
    tree_end =[]
    for tree in treesequence.trees():
        tree_start.append(tree.interval.left)
        tree_end.append(tree.interval.right)
    tree_tab=[tree_start,tree_end]
    return tree_tab
    

# Function counting polytomies
def polytomies(treesequence): #Renvoie liste du nombre de polytomies par arbre dans un treesequence
	count_polytomies = []
	for tree in treesequence.trees(): #Loop on trees of the tree_sequence
		count = 0
		for n in tree.nodes(): # Loop on the nodes of the tree
			if len(tree.children(n)) > 2:
				count += 1
		count_polytomies.append(count)
	return count_polytomies

# Function computing Sackin Index
def sackin(treesequence,pos_list):
	sack = []
	for pos in pos_list:
		sack.append(treesequence.at(pos).sackin_index()/(len(list(treesequence.at(pos).nodes()))-(len(list(treesequence.at(pos).leaves()))+1)))
	return sack


# Function calculating B2 Index
def b2(treesequence,pos_list):
	b = []
	for pos in pos_list:
		b.append(treesequence.at(pos).b2_index()/(len(list(treesequence.at(pos).nodes()))-(len(list(treesequence.at(pos).leaves()))+1)))
	return b

